package mempool

import (
	"sync"
)

type Pool[T any] struct {
	sync.Mutex
	slc []T
	idx int
	New func() T
}

func (p *Pool[T]) Get() (item T) {
	p.Lock()
	if p.idx == 0 { //slc empty
		var v T
		p.slc = append(p.slc, v)
		p.Unlock()
		println("new item")
		return p.New()
	}
	p.idx--
	item = p.slc[p.idx]
	p.Unlock()
	print("get item. ")
	return
}
func (p *Pool[T]) Put(item T) {
	p.Lock()
	p.slc[p.idx] = item
	p.idx++
	p.Unlock()
	print("put item. ")
}
