package main

import (
	"time"
	"unsafe"

	"gitlab.com/birowo/mempool"
)

func StrSlc(slc []byte) string {
	return *(*string)(unsafe.Pointer(&slc))
}
func main() {
	p := &mempool.Pool[[]byte]{
		New: func() []byte {
			return make([]byte, 16)
		},
	}
	for i := 0; i < 999; i++ {
		go func() {
			test1 := "test_123."
			test1Len := len(test1)
			buf := p.Get()
			if test1Len < len(buf) {
				copy(buf, test1)
				print(StrSlc(buf[:test1Len]), " ")
			}
			p.Put(buf)
			test2 := "qwertyuiop."
			test2Len := len(test2)
			buf = p.Get()
			if test2Len < len(buf) {
				copy(buf, test2)
				print(StrSlc(buf[:test2Len]), " ")
			}
			p.Put(buf)
		}()
	}
	time.Sleep(9 * time.Second)
	println()
}
