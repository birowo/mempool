# memory pool

#GOLANG

memory pool

run with -race flag to check race condition . CGO_ENABLED env. var. should be set for -race flag to work .

![mempool](https://user-images.githubusercontent.com/21541959/146319109-1ff9f8d3-df9b-45ec-8ed5-e8a0944e10b4.png)

https://golang.org/doc/articles/race_detector#How_To_Use
